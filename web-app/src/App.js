import Layout from './layouts/Layout';
import themes from './themes/themes';
import { ThemeProvider } from '@mui/material';
import './styles/styles.scss';

function App() {
  return (
    <>
      <ThemeProvider theme={themes}>
        <Layout />
      </ThemeProvider>
    </>
  );
}

export default App;
