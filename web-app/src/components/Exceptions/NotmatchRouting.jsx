import { Box, Typography } from '@mui/material'
import React from 'react'

const NotMatchRouting = () => {
  return (
    <Box sx={{display: 'flex', justifyContent: 'center'}}>
        <Typography variant='subtitle2'>Not found 404!</Typography>
    </Box>
  )
}

export default NotMatchRouting