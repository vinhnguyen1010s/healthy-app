import { Box, CardMedia } from '@mui/material';
import React from 'react';

const ItemFood = (props) => {
  const {} = props;

  return (
    <>
      <Box>
        <CardMedia sx={{ height: '100%', width: '100%', backgroundSize: 'cover' }} image="assets/images/imgs/d01.jpg" />
      </Box>
    </>
  );
};

export default ItemFood;
