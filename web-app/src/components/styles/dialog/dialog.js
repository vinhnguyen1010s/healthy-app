import theme, { colors } from '@/themes/theme';
import { Dialog, DialogActions, DialogContent, Grid, Typography } from '@mui/material';
import { styled } from '@mui/system';

const CDialog = styled(Dialog)({
    '& .MuiPaper-root': {
        maxWidth: '910px',
        width: '100%',
    },
});

const CDialogTitle = styled(Grid)({
    padding: '16px 32px',
    minHeight: '72px',
    borderBottom: '1px solid ' + colors.line2,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
});

const CDialogBottom = styled(Grid)({
    padding: '16px 32px',
    minHeight: '72px',
    borderTop: '1px solid ' + colors.line2,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
});

const CDialogContent = styled(DialogContent)({
    padding: '12px 32px',
});

const CDialogTitleLeft = styled(Typography)({
    fontSize: '18px',
    fontWeight: '600',
    color: theme.palette.text.main,
});

const CDialogTitleRight = styled(Grid)({
    '&>:not(:first-of-type)': {
        marginLeft: '8px',
    },
});

const CDialogActions = styled(DialogActions)({
    borderTop: '1px solid ' + colors.line2,
    padding: '16px 32px',
    '& .btn-cancel': {
        height: '40px',
        padding: '6px 28px',
        fontWeight: '500',
    },
    '& .btn-save': {
        height: '40px',
        padding: '6px 44px',
        fontWeight: '500',
    },
});

export { CDialog, CDialogTitle, CDialogTitleLeft, CDialogTitleRight, CDialogActions, CDialogContent, CDialogBottom };
