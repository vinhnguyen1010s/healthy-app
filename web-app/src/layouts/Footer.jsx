import React from 'react';
import './layout.scss';
import { Box, Grid, Stack, Typography } from '@mui/material';
import { colors } from '../themes/themes';

const Footer = () => {
  return (
    <>
      <Box sx={{ height: '100%', flexGrow: 1 }}>
        <Box sx={{ height: '100%', display: 'flex', gap: 3, alignItems: 'center', color: colors.colorText }}>
          <Box>
            <Typography variant="subtitle5">{'会員登録'}</Typography>
          </Box>
          <Box>
            <Typography variant="subtitle5">{'運営会社'}</Typography>
          </Box>
          <Box>
            <Typography variant="subtitle5">{'利用規約'}</Typography>
          </Box>
          <Box>
            <Typography variant="subtitle5">{'個人情報の取扱について'}</Typography>
          </Box>
          <Box>
            <Typography variant="subtitle5">{'特定商取引法に基づく表記'}</Typography>
          </Box>
          <Box sx={{}}>
            <Typography variant="subtitle5">{'お問い合わせ'}</Typography>
          </Box>
        </Box>
      </Box>
    </>
  );
};

export default Footer;
