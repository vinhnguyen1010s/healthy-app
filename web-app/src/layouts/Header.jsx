import { AppBar, Badge, Grid, IconButton, ListItem, ListItemIcon, Toolbar, Typography } from '@mui/material';
import { Box } from '@mui/system';
import React from 'react';
import { Link, NavLink } from 'react-router-dom';
import { useStyles } from './layoutStyles';
import './layout.scss';
import logo from '../assets/images/logo.svg';
import challenge from '../assets/images/icons/icon_challenge.svg';
import memo from '../assets/images/icons/icon_memo.svg';
import info from '../assets/images/icons/icon_info.svg';
import { styled } from '@mui/material/styles';
import MuiListItemText from '@mui/material/ListItemText';
import MenuIcon from '@mui/icons-material/Menu';

export const navLinks = [
  { title: `自分の記録`, path: `/my-record`, icon: memo, badge: false },
  { title: `チャレンジ`, path: `/challenge`, icon: challenge, badge: false },
  { title: `お知らせ`, path: `/notices`, icon: info, badge: true },
];

const ListItemText = styled(MuiListItemText)({
  '& .MuiListItemText-primary': {
    fontWeight: 300,
  },
});

const Header = () => {
  const classes = useStyles();
  const links = navLinks;

  return (
    <>
      <AppBar position="fixed" elevation={0} className={classes.appBar}>
        <Toolbar className={classes.toolbar}>
          <Link to="/" className={classes.typography}>
            <Typography component="img" sx={{ px: 2, py: 1 }} src={logo} className="logo-app"></Typography>
          </Link>

          <Box className="navlinks">
            <nav className={classes.navDisplayFlex}>
              {links.map((item, index) => (
                <NavLink className={classes.navItems} to={item.path} key={index}>
                  <ListItem className={classes.linkItem} key={item.index}>
                    <ListItemIcon className="icon-challenge">
                      {item.badge ? (
                        <Badge badgeContent={4} color="primary">
                          <img src={item.icon} />
                        </Badge>
                      ) : (
                        <img src={item.icon} />
                      )}
                    </ListItemIcon>
                    <ListItemText primary={item.title} />
                  </ListItem>
                </NavLink>
              ))}
              <Box sx={{ pl: 2 }}>
                <IconButton>
                  <MenuIcon color="primary"></MenuIcon>
                </IconButton>
              </Box>
            </nav>
          </Box>
        </Toolbar>
      </AppBar>
    </>
  );
};

export default Header;
