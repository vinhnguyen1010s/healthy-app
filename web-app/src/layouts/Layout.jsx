import { Box } from '@mui/material';
import React, { Suspense } from 'react';
import { Route, BrowserRouter as Router, Routes } from 'react-router-dom';
import { LoadingSpinner } from '../components/LoadingSpinner';
import { componentRoutes } from '../routers/Routes';
import Header from './Header';
import { useStyles } from './layoutStyles';
import './layout.scss';
import Footer from './Footer';

const Layout = () => {
  const classes = useStyles();
  const routes = componentRoutes;

  const TopPage = React.lazy(() => import('../modules/topPage/TopPage'));
  const Notmatch = React.lazy(() => import('../components/Exceptions/NotmatchRouting'));

  return (
    <>
      <Router>
        <Header />

        <Box className={classes.content}>
          <Suspense fallback={<LoadingSpinner />}>
            <Routes>
              {routes.map((route, index) => {
                return <Route key={index} path={route.path} element={route.component} />;
              })}
              <Route index path="/" element={<TopPage />} />
              <Route path="*" element={<Notmatch />} />
            </Routes>
          </Suspense>
        </Box>

        <Box className={classes.footer}>
          <Footer></Footer>
        </Box>
      </Router>
    </>
  );
};

export default Layout;
