import { Box, Button, CardMedia, IconButton, Typography } from '@mui/material';
import { makeStyles } from '@mui/styles';
import Grid from '@mui/system/Unstable_Grid';
import React, { useEffect, useState } from 'react';
import CustomCircular from '../../components/chart/CustomCircular';
import LineChart from '../../components/chart/LineChart';
import themes, { colors } from '../../themes/themes';

const useStyles = makeStyles(() => ({
  contentPage: {
    flexGrow: 1,
    display: 'block',
    padding: themes.spacing(3, 20),

    [themes.breakpoints.down('md')]: {
      padding: themes.spacing(3, 4),
    },
  },

  titleItem: {
    position: 'relative',
    bottom: '30px',
    left: 0,
    padding: '2px 4px',
    color: colors.colorText,
    backgroundColor: colors.light,
  },
}));

const data = {
  res: [
    {
      id: 1,
      title: 'test 1',
      img_url: '/assets/images/imgs/d01.jpg',
    },
  ],
};

const TopPage = () => {
  const classes = useStyles();
  const [isLoading, setisLoading] = useState(false);
  const dataChart = {
    data: {
      index_num: 75,
      time: '05.21',
      point_time: 'Lunch',
      colaries: [110, 100, 80, 78, 65, 60, 50, 45, 40, 32, 26, 12],
      kcal: [110, 105, 70, 88, 75, 70, 60, 75, 60, 55, 50, 58],
    },
  };

  const buttons = [
    { id: 1, imageUrl: 'assets/images/icons/dinner.svg', name: 'morning' },
    { id: 2, imageUrl: 'assets/images/icons/lunch.svg', name: 'lunch' },
    { id: 3, imageUrl: 'assets/images/icons/dinner.svg', name: 'dinner' },
    { id: 4, imageUrl: 'assets/images/icons/snack.svg', name: 'snack' },
  ];

  const dataTopPage = [
    {
      id: 1,
      point_time: 'Morning',
      time: '05:21',
      imageUrl: 'assets/images/imgs/m01.jpg',
    },
    {
      id: 2,
      point_time: 'Morning',
      time: '05:21',
      imageUrl: 'assets/images/imgs/m02.jpg',
    },

    {
      id: 3,
      point_time: 'Dinner',
      time: '05:21',
      imageUrl: 'assets/images/imgs/d01.jpg',
    },

    {
      id: 4,
      point_time: 'Dinner',
      time: '05:21',
      imageUrl: 'assets/images/imgs/d02.jpg',
    },

    {
      id: 5,
      point_time: 'Lunch',
      time: '05:21',
      imageUrl: 'assets/images/imgs/l01.jpg',
    },

    {
      id: 6,
      point_time: 'Lunch',
      time: '05:21',
      imageUrl: 'assets/images/imgs/l02.jpg',
    },

    {
      id: 7,
      point_time: 'Lunch',
      time: '05:21',
      imageUrl: 'assets/images/imgs/l03.jpg',
    },

    {
      id: 8,
      point_time: 'Snack',
      time: '05:21',
      imageUrl: 'assets/images/imgs/s01.jpg',
    },
    {
      id: 9,
      point_time: 'Lunch',
      time: '05:21',
      imageUrl: 'assets/images/imgs/l03.jpg',
    },
    {
      id: 10,
      point_time: 'Snack',
      time: '05:21',
      imageUrl: 'assets/images/imgs/s01.jpg',
    },
  ];

  const [listData, setlistData] = useState([]);

  useEffect(() => {
    // first;
    setlistData(dataTopPage);

    return () => {
      // second;
    };
  }, []);

  return (
    <>
      <Box sx={{ flexGrow: 1 }}>
        {}
        <Box sx={{ display: 'flex', height: 312 }}>
          <Box sx={{ width: '42%' }}>
            <CardMedia
              sx={{ height: '100%', width: '100%', backgroundSize: 'cover' }}
              image="assets/images/imgs/d01.jpg"
            />
            <CustomCircular data={dataChart} />
          </Box>
          <Box sx={{ p: 2, display: 'flex', justifyContent: 'center', width: '58%', backgroundColor: colors.line1 }}>
            <LineChart data={dataChart}></LineChart>
          </Box>
        </Box>

        <Box sx={{ display: 'flex', justifyContent: 'center', mt: 3 }}>
          <Box sx={{ display: 'flex', gap: 5 }}>
            {buttons.map((item) => (
              <IconButton key={item.id} style={{ maxWidth: '136px', height: 'auto' }}>
                <img style={{ width: '100%', height: '100%' }} src={item.imageUrl} alt={item.alt} />
              </IconButton>
            ))}
          </Box>
        </Box>

        <Box className={classes.contentPage}>
          <Grid container spacing={1}>
            {listData.map((item, index) => (
              <Grid xs={4} sm={3} key={index}>
                <Box sx={{ width: '100%', height: '100%' }}>
                  <img
                    src={item.imageUrl}
                    alt={item.point_time}
                    style={{ width: '100%', height: '100%', maxWidth: '100%', maxHeight: '100%', objectFit: 'cover' }}
                  />
                  <Typography variant="subtitle9" className={classes.titleItem}>
                    {`${item.time}:${item.point_time}`}
                  </Typography>
                </Box>
              </Grid>
            ))}
          </Grid>
        </Box>

        <Box sx={{ display: 'flex', justifyContent: 'center', mb: 8 }}>
          <Button variant="contained" sx={{ p: 2, width: 296 }}>
            {'記録をもっと見る'}
          </Button>
        </Box>
      </Box>
    </>
  );
};

export default TopPage;
