import React from 'react';

const MyRecord = React.lazy(() => import('../modules/myRecord/MyRecord'));
const TopPage = React.lazy(() => import('../modules/topPage/TopPage'));
const ColumnPage = React.lazy(() => import('../modules/columnPage/ColumnPage'));
const Notices = React.lazy(() => import('../modules/notices/Notices'));
const Challenge = React.lazy(() => import('../modules/challenge/Challenge'));

export const componentRoutes = [
  {
    path: '/top-page',
    component: <TopPage />,
  },
  {
    path: '/my-record',
    component: <MyRecord />,
  },
  {
    path: '/notices',
    component: <Notices />,
  },
  {
    path: '/challenge',
    component: <Challenge />,
  },
];

export const recordRouting = [
  {
    path: '/my-record/column-page',
    component: <ColumnPage />,
  },
];
