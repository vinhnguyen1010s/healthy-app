const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { ProvidePlugin } = require('webpack');

module.exports = {
  entry: path.resolve(__dirname, 'src', 'index.js'), // Dẫn tới file index.js ta đã tạo
  output: {
    path: path.resolve(__dirname, 'dist'), // Thư mục chứa file được build ra
    filename: '[name].js', // Tên file được build ra
  },
  mode: 'development',
  devServer: {
    port: 3000,
    historyApiFallback: true,
    headers: {
      'Access-Control-Allow-Origin': '*',
    },
  },
  resolve: {
    extensions: ['*', '.js', '.jsx', '.ts', '.tsx', '.json'],
  },
  module: {
    rules: [
      {
        test: /\.js$|jsx/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        options: {
          presets: ['@babel/preset-env', '@babel/preset-react'],
        },
      },
      {
        test: /\.json$/,
        exclude: /node_modules/,
        use: 'json-loader',
      },
      {
        test: /\.(sa|sc|c)ss$/, // Sử dụng style-loader, css-loader cho file .css
        use: ['style-loader', 'css-loader', 'sass-loader'],
      },
      {
        test: /\.(png|jpg|jpeg|gif|woff|woff2|eot|otf|ttf|svg|ico)$/, // to import images and fonts
        loader: 'url-loader',
        // use: [
        //   {
        //     loader: 'file-loader',
        //   },
        // ],
      },
    ],
  },
  plugins: [
    new ProvidePlugin({
      React: 'react',
    }),
    new HtmlWebpackPlugin({
      template: './public/index.html',
    }),
  ],
};
